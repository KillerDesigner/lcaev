# Low-Cost Automated Emergency Ventilator

This project is intended to provide a low-cost, easy to produce, automated ventilator 
FOR EMERGENCY USE ONLY WHEN NO MEDICAL DEVICES ARE AVAILABLE.  This is to be
used ONLY as an alternative to death, not to certified medical equipment.  This code
and instruction set is released as-is with the user assuming all risks and
agreeing to hold all contributors harmless.  This is a desperate measure for a 
desperate time.

## Hardware Setup

* Raspberry Pi 4 (3B+ would probably also work)
* compatible 7" + touchscreen (ideally plug-n-play)
* NEMA 17 stepper motor
* Adafruit Pi Stepper Controller
* TBR speakers/sensors
* TBR 3D printed components

## Software Setup

1. Write the latest Raspbian to a microSD card
2. Clone the LCAEV repository
3. Go to setup/ and run install.py
4. Restart the pi
5. Probably should add the main GUI to rc.local


## Use

1. The GUI should come up at boot and away you go
2. Select settings as appropriate to initiate treatment


## License
Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
