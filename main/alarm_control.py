#!/usr/bin/python3

'''
alarm_control.py

This program is intended to control 
alarms

python3


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import sys
import os
import time
#import pygame
import json



###########################
#SUPPORT FUNCTIONS
###########################


def alarm_operate():

    print('starting main alarm function')


    #set vars
    start_time = time.time()
    time_interval = 1
    mode = 'IDLE'
    mute = 0
    sound_alarm = 1 #should be 0 when it's actually hooked up

    sound_filename = 'alarm_short.wav'
    file_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))


    while True:


        if mode == 'CHECKOUT':
            print('running alarm checkout...')
            #pygame.mixer.music.play()

            os.system('mplayer -nolirc ' + os.path.join(file_dir, sound_filename))
            mode = 'IDLE'
            time.sleep(5) #should eventually be 1


        if mode == 'OPERATE':
            print('alarm active')

            if sound_alarm > mute:
                os.system('mplayer -nolirc ' + os.path.join(file_dir, sound_filename))
                pass



        #check for new inputs
        if time.time()-start_time > time_interval:

            try:
                with open(os.path.join(file_dir,'alarm_data.json')) as f:
                    data = json.load(f)
                mode = data['mode']
                mute = int(data['mute'])


            except: pass

            start_time = time.time()



        time.sleep(1) #should eventually be 1


def beep():

    #import pygame
    #pygame.mixer.init()
    #pygame.mixer.music.load('alarm_short.wav')
    #print('starting demo alarm code')
    #pygame.mixer.music.play()

    print('beep')
    os.system('mplayer -nolirc ' + os.path.join(file_dir, sound_filename))




###########################
#MAIN CODE
###########################

#pygame.mixer.init()
#pygame.mixer.music.load('alarm_short.wav')

if __name__ == '__main__':


    
    try:
        start = sys.argv[1]
        if start == 'MAIN': alarm_operate()
    except:
        beep()

    
    #alarm_operate()



