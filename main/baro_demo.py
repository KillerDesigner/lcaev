#!/usr/bin/python3

'''
baro_demo.py

This program is intended to connect
to a BMP180 and read some data

Set up for a python3 on a pi 4

'''

###########################
#IMPORT MODULES
###########################
import os
import csv
import time
import json
import Adafruit_BMP.BMP085 as BMP085



###########################
#MAIN CODE
###########################

#vars
baro = BMP085.BMP085(mode=BMP085.BMP085_ULTRAHIGHRES)
time_interval = 30
tdata = []
pdata = []
ttime = []
the_start = time.time()
n = 0
start_time = time.time()

while True:

    ttime += [time.time()-the_start]
    pdata += [baro.read_pressure()]
    tdata += [baro.read_temperature()]
    print('\n----------------------------------')
    print(pdata[-1])
    print(tdata[-1])
    time.sleep(0.1)


    #check for new inputs
    if time.time()-start_time > time_interval:

        n+=1
        data = {'time':ttime,'Pressure':pdata,'Temp':tdata}

        keys = sorted(data.keys())
        with open(os.path.join('TEST_BARO_DATA_'+str(n)+'.csv'),'wb') as csv_file:
             writer=csv.writer(csv_file)
             writer.writerow(keys)  
             writer.writerows(zip(*[data[key] for  key in keys]))



        start_time = time.time()
        tdata = []
        pdata = []
        ttime = []



