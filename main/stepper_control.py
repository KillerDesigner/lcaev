#!/usr/bin/python3

'''
stepper_control.py

This program is intended to control 
stepper motors via the adafruit
stepper shield for Pis

python3


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import sys
import os
import time
import json
import atexit
from adafruit_motorkit import MotorKit
from adafruit_motor import stepper




###########################
#SUPPORT FUNCTIONS
###########################

# release/turn off motors at exit
def turnOffMotors():
    kit.stepper1.release()
    kit.stepper2.release()


def extend(kitstepper1,kitstepper2,steps,delay):
    for i in range(steps):
        kitstepper1.onestep(style = stepper.DOUBLE)
        time.sleep(delay) #move delays in between to increase spacing between
        kitstepper2.onestep(style = stepper.DOUBLE)


def retract(kitstepper1,kitstepper2,steps,delay):
    for i in range(steps):
        kitstepper1.onestep(direction=stepper.BACKWARD, style = stepper.DOUBLE)
        time.sleep(delay) #move delays in between to increase spacing between
        kitstepper2.onestep(direction=stepper.BACKWARD, style = stepper.DOUBLE)


def stepper_operate(kitstepper1,kitstepper2):

    print('starting main stepper function')

    #set vars
    start_time = time.time()
    file_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    time_interval = 1 #seconds between settings changes
    mode = 'IDLE'
    resp_rate = 10
    tidal_vol = 500
    max_vol = 660 #UPDATE ME
    peep = 10
    full_stroke_steps = 190
    ItoE_ratio= 0.5
    delay = 0.001  #was 0.001

    while True:

        if mode == 'CHECKOUT':
            print('running stepper checkout...')

            steps = full_stroke_steps

            start_time = time.time()
            extend(kitstepper1,kitstepper2,steps,delay)
            stroke_in_time = time.time()-start_time

            print('stroke took: ' + str(stroke_in_time)+' s')

            time.sleep(0.5)

            steps = full_stroke_steps
            start_time = time.time()
            retract(kitstepper1,kitstepper2,steps,delay)
            stroke_out_time = time.time()-start_time

            print('stroke took: ' + str(stroke_out_time)+' s')

            #release rotor
            kitstepper1.release()
            kitstepper2.release()

            #stroke time
            stroke_time = (stroke_in_time+stroke_out_time)/2

            mode = 'IDLE'


        if mode == 'OPERATE':

            #math out the rest of the params
            exhal_time = stroke_time*2
            total_breath_time = stroke_time+exhal_time
            rr_delay = (60/resp_rate)-stroke_time-exhal_time

            if rr_delay < 0: rr_delay = 0

            the_steps = int((full_stroke_steps/max_vol)*tidal_vol)

            #run w/ pause for appropriate RR
            extend(kitstepper1,kitstepper2,the_steps,delay)
            #maybe should put a tiny pause here?
            retract(kitstepper1,kitstepper2,the_steps,delay)

            #release rotor at end of stroke
            starttt = time.time()
            kitstepper1.release()
            kitstepper2.release()

            if rr_delay > 3:
                remainder = rr_delay - int(rr_delay)
                n = 0
                while n < rr_delay:
                    time.sleep(1)
                    try:
                        with open(os.path.join(file_dir,'stepper_data.json')) as f:
                            data = json.load(f)
                        mode = data['mode']
                        resp_rate = int(data['resp_rate'])
                        tidal_vol = int(data['tidal_vol'])
                        peep = int(data['peep'])

                    except: pass

                    rr_delay = (60/resp_rate)-stroke_time-exhal_time
                    n+=1

                time.sleep(remainder)
            else:
                time.sleep(rr_delay)





        else: #idle
            time.sleep(1)


        #check for new inputs
        if time.time()-start_time > time_interval:

            try:
                with open(os.path.join(file_dir,'stepper_data.json')) as f:
                    data = json.load(f)
                mode = data['mode']
                resp_rate = int(data['resp_rate'])
                tidal_vol = int(data['tidal_vol'])
                peep = int(data['peep'])

            except: pass

            start_time = time.time()



def cal_stroke(kitstepper1,kitstepper2):

    print('starting stepper cal routine')


    #define vars
    full_stroke_steps = 190
 

    steps = full_stroke_steps
    delay = 0.001
    start_time = time.time()
    extend(kitstepper1,kitstepper2,steps,delay)
    stroke_in_time = time.time()-start_time

    print('stroke took: ' + str(stroke_in_time)+' s')

    time.sleep(0.5)

    steps = full_stroke_steps
    delay = 0.001
    start_time = time.time()
    retract(kitstepper1,kitstepper2,steps,delay)
    stroke_out_time = time.time()-start_time

    print('stroke took: ' + str(stroke_out_time)+' s')

    #release rotor
    kitstepper1.release()
    kitstepper2.release()

    #stroke time
    stroke_time = (stroke_in_time+stroke_out_time)/2

    return stroke_time, full_stroke_steps


###########################
#MAIN CODE
###########################

# initialize stepper
kit = MotorKit()

#release rotor
kit.stepper1.release()
kit.stepper2.release()

atexit.register(turnOffMotors)

if __name__ == '__main__':

    try:
        start = sys.argv[1]
        if start == 'MAIN':  stepper_operate(kit.stepper1,kit.stepper2)
    except:
        while True:
            cal_stroke(kit.stepper1,kit.stepper2)



