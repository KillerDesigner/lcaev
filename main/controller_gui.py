#!/usr/bin/python3

'''
controller_gui.py

This program is intended to provide a
Graphical User Interface so users
can operate the LCAEV

built for RPi4 with a 7" 1920x1080
downscaled screen

python3


Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import sys
import os
import time
import json
#from Tkinter import * python 2
from tkinter import * #python 3
import subprocess

try: import stepper_control
except:
    print('WARNING: unable to import stepper control')
    pass


###########################
#CLASSES
###########################

#GUI class
class theGUI:

    def __init__(self, master):

        # set vars
        self.master = master
        master.title("EMERGENCY VENTILATOR CONTROL SYSTEM")
        self.GO_DICT = {'test':False}
        self.ARMED_STATUS = False
        self.standard_font = 'Helvetica 100 bold'
        self.sorta_small_font = 'Helvetica 60 bold'
        self.small_font = 'Helvetica 30 bold'
        self.huge_font = 'Helvetica 150 bold'
        self.language_options = ['English','Placeholder (Also English)']

        # SET VENTILATOR VARS
        self.RUNNING = False
        self.RR = 10
        self.Vt = 500
        self.PEEP = 10
        self.MUTE = 0
        self.stepper_mode = 'IDLE'
        self.alarm_mode = 'IDLE'


       # creating a menu instance
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.master.quit)

        # added "file" to our menu
        menu.add_cascade(label="File", menu=file)

        #full file dir
        self.full_file_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

        # clean prior config files and create new ones
        try: os.remove(os.path.join(self.full_file_dir,'stepper_data.json'))
        except: pass

        try: os.remove(os.path.join(self.full_file_dir,'alarm_data.json'))
        except: pass

        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'stepper_data.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'alarm_data.json'), 'w') as f:
            json.dump(alarm_dict, f)



        #start support scripts
        subprocess.Popen(['python3 '+os.path.join(self.full_file_dir,'stepper_control.py ') + 'MAIN'], shell=True, stdin=None, stdout=None, stderr=None)
        subprocess.Popen(['python3 '+os.path.join(self.full_file_dir,'alarm_control.py ') + 'MAIN'], shell=True, stdin=None, stdout=None, stderr=None)

        # draw language selection page
        self.draw_language_prompt()


    # step 1, language select
    def draw_language_prompt(self):

        #spacer
        roww = 0
        self.label_text = StringVar()
        self.label_text.set("            ")
        self.spacer1 = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer1.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = StringVar()
        self.label_text.set("Select Language")
        self.lang_select_label = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.lang_select_label.grid(row=roww,column=1)

        roww+=1
        self.language_var = StringVar()
        self.language_var.set(self.language_options[0]) # default value
        self.language_menu = OptionMenu(self.master, self.language_var, *self.language_options)
        self.language_menu.grid(row=roww,column=1)
        self.language_menu.configure(font=self.standard_font)
        self.language_menu['menu'].configure(font=self.standard_font)

        roww+=1
        self.label_text = StringVar()
        self.label_text.set("  ")
        self.spacer2 = Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.spacer2.grid(row=roww,column=0)

        roww+=1
        self.lang_confirm_button = Button(self.master, text="CONFIRM", font=self.standard_font, command= self.lang_confirm_button_cmd)
        self.lang_confirm_button.grid(row=roww,column=1)
        self.lang_confirm_button.config(bg='light green')


    def lang_confirm_button_cmd(self):

        #need to clean up all widgets and then call next draw
        self.language_menu.grid_forget()
        self.lang_select_label.grid_forget()
        self.lang_confirm_button.grid_forget()
        self.spacer1.grid_forget()
        self.spacer2.grid_forget()

        self.draw_calibration_window()


    # step 2, calibrate sensors
    def draw_calibration_window(self):

        #spacer
        roww = 0
        self.label_text = StringVar()
        self.label_text.set("       ")
        self.spacer3 = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer3.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = StringVar()
        self.label_text.set("CALIBRATION:")
        self.cal_caution_label = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.cal_caution_label.grid(row=roww,column=1)
        self.cal_caution_label.config(bg='light yellow')

        roww+=1
        self.label_text = StringVar()
        self.label_text.set("ENSURE SYSTEM IS DISCONNECTED FROM PATIENT")
        self.cal_caution_label2 = Label(self.master, textvariable=self.label_text, font=self.small_font)
        self.cal_caution_label2.grid(row=roww,column=1)
        self.cal_caution_label2.config(bg='light yellow')

        roww+=1
        self.label_text = StringVar()
        self.label_text.set(" ")
        self.spacer4 = Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.spacer4.grid(row=roww,column=0)

        roww+=1
        self.cal_confirm_button = Button(self.master, text="START CALIBRATION", font=self.standard_font, command= self.cal_confirm_button_cmd)
        self.cal_confirm_button.grid(row=roww,column=1)
        self.cal_confirm_button.config(bg='light green')


    def cal_confirm_button_cmd(self):

        #need to clean up all widgets and then call next draw
        self.spacer3.grid_forget()
        self.spacer4.grid_forget()
        self.cal_caution_label.grid_forget()
        self.cal_caution_label2.grid_forget()
        self.cal_confirm_button.grid_forget()

        self.draw_self_test_start_window()



    # step 3, self-test start
    def draw_self_test_start_window(self):

        #spacer
        roww = 0
        self.label_text = StringVar()
        self.label_text.set("          ")
        self.spacer5 = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer5.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = StringVar()
        self.label_text.set("Unit Self Test")
        self.self_test_label = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.self_test_label.grid(row=roww,column=1)

        roww+=1
        self.label_text = StringVar()
        self.label_text.set("ENSURE SYSTEM IS DISCONNECTED FROM PATIENT")
        self.self_test_label2 = Label(self.master, textvariable=self.label_text, font=self.small_font)
        self.self_test_label2.grid(row=roww,column=1)
        self.self_test_label2.config(bg='light yellow')

        roww+=1
        self.label_text = StringVar()
        self.label_text.set("  ")
        self.spacer6 = Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.spacer6.grid(row=roww,column=0)

        roww+=1
        self.self_test_start_button = Button(self.master, text="START SELF TEST", font=self.standard_font, command= self.test_start_button_cmd)
        self.self_test_start_button.grid(row=roww,column=1)
        self.self_test_start_button.config(bg='light green')


    def test_start_button_cmd(self):

        #need to clean up all widgets and then call next draw
        self.spacer5.grid_forget()
        self.spacer6.grid_forget()
        self.self_test_label.grid_forget()
        self.self_test_label2.grid_forget()
        self.self_test_start_button.grid_forget()

        #subprocess the test procedures
        self.stepper_mode = 'CHECKOUT'
        self.alarm_mode = 'CHECKOUT'
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'stepper_data.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'alarm_data.json'), 'w') as f:
            json.dump(alarm_dict, f)

        #move on to next window
        self.draw_self_test_complete_window()



    # step 3, self-test complete
    def draw_self_test_complete_window(self):

        #spacer
        roww = 0
        self.label_text = StringVar()
        self.label_text.set("           ")
        self.spacer7 = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer7.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = StringVar()
        self.label_text.set("IS AIR FLOWING?")
        self.self_test_label3 = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.self_test_label3.grid(row=roww,column=1)

        '''
        roww+=1
        self.label_text = StringVar()
        self.label_text.set("IS AIR FLOWING?")
        self.self_test_label4 = Label(self.master, textvariable=self.label_text, font=self.small_font)
        self.self_test_label4.grid(row=roww,column=1)
        self.self_test_label4.config(bg='light yellow')
        '''

        roww+=1
        self.label_text = StringVar()
        self.label_text.set("  ")
        self.spacer8 = Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.spacer8.grid(row=roww,column=0)

        roww+=1
        self.self_test_good_button = Button(self.master, text="YES", font=self.standard_font, command= self.test_pass_button_cmd)
        self.self_test_good_button.grid(row=roww,column=1)
        self.self_test_good_button.config(bg='light green')

        roww+=1
        self.self_test_fail_button = Button(self.master, text="NO", font=self.standard_font, command= self.test_fail_button_cmd)
        self.self_test_fail_button.grid(row=roww,column=1)
        self.self_test_fail_button.config(bg='red')


    def test_pass_button_cmd(self):

        #return systems to idle
        self.stepper_mode = 'IDLE'
        self.alarm_mode = 'IDLE'
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'stepper_data.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'alarm_data.json'), 'w') as f:
            json.dump(alarm_dict, f)


        #need to clean up all widgets and then call next draw
        self.spacer7.grid_forget()
        self.spacer8.grid_forget()

        self.self_test_label3.grid_forget()
        #self.self_test_label4.grid_forget()
        self.self_test_good_button.grid_forget()
        self.self_test_fail_button.grid_forget()

        self.draw_self_test_pass_window()


    def test_fail_button_cmd(self):

        #need to clean up all widgets and then call next draw
        self.self_test_label3.grid_forget()
        #self.self_test_label4.grid_forget()
        self.self_test_good_button.grid_forget()
        self.self_test_fail_button.grid_forget()

        self.draw_test_fail_window()


    # step fail window
    def draw_test_fail_window(self):
        print('CRITICAL FAILURE')
        #spacer
        roww = 0
        self.label_text = StringVar()
        self.label_text.set("               ")
        self.label = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.label.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = StringVar()
        self.label_text.set("UNIT FAILURE")
        self.self_test_label = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.self_test_label.grid(row=roww,column=1)
        self.self_test_label.config(bg='red')

        roww+=1
        self.label_text = StringVar()
        self.label_text.set("DO NOT USE -- GET SERVICED IMMEDIATELY")
        self.self_test_label2 = Label(self.master, textvariable=self.label_text, font=self.small_font)
        self.self_test_label2.grid(row=roww,column=1)
        self.self_test_label2.config(bg='light yellow')

        roww+=1
        self.label_text = StringVar()
        self.label_text.set("  ")
        self.label = Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.label.grid(row=roww,column=0)


    # draw test pass window
    def draw_self_test_pass_window(self):

        #spacer
        roww = 0
        self.label_text = StringVar()
        self.label_text.set("                ")
        self.spacer9 = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer9.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = StringVar()
        self.label_text.set("TEST PASS")
        self.self_test_label3 = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.self_test_label3.grid(row=roww,column=1)

        roww+=1
        self.label_text = StringVar()
        self.label_text.set("READY TO OPERATE")
        self.self_test_label4 = Label(self.master, textvariable=self.label_text, font=self.small_font)
        self.self_test_label4.grid(row=roww,column=1)
        self.self_test_label4.config(bg='light yellow')

        roww+=1
        self.label_text = StringVar()
        self.label_text.set("  ")
        self.spacer10 = Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.spacer10.grid(row=roww,column=0)

        roww+=1
        self.self_test_next_button = Button(self.master, text="NEXT", font=self.standard_font, command= self.draw_operate_screen)
        self.self_test_next_button.grid(row=roww,column=1)
        self.self_test_next_button.config(bg='light green')


    # draw operate screen
    def draw_operate_screen(self):

        # clean up prior screens
        self.spacer9.grid_forget()
        self.spacer10.grid_forget()

        self.self_test_label3.grid_forget()
        self.self_test_label4.grid_forget()
        self.self_test_next_button.grid_forget()

        #draw screen, first row
        #spacers 
        roww = 0
        self.label_text = StringVar()
        self.label_text.set("     ")
        self.spacer11 = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer11.grid(row=roww,column=0)

        self.label_text = StringVar()
        self.label_text.set("    ")
        self.spacer12 = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer12.grid(row=roww,column=3)

        self.mute_button = Button(self.master, text="MUTE", font=self.sorta_small_font, command= self.mute_alarm)
        self.mute_button.grid(row=roww,column=6)
        self.mute_button.config(bg='blue')
        self.mute_button.config(fg='white')


        #draw second row
        roww+=1
        self.label_text = StringVar()
        self.label_text.set("RR")
        self.label = Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.label.grid(row=roww,column=1)

        self.label_RR_text = StringVar()
        self.label_RR_text.set(str(self.RR))
        self.label_RR = Label(self.master, textvariable=self.label_RR_text,font=self.standard_font, relief=SUNKEN)
        self.label_RR.grid(row=roww,column=2)
        self.label_RR.config(bg='white')


        self.label_text = StringVar()
        self.label_text.set("Vt")
        self.label = Label(self.master, textvariable=self.label_text,font=self.standard_font)
        self.label.grid(row=roww,column=4)

        self.label_Vt_text = StringVar()
        self.label_Vt_text.set(str(self.Vt))
        self.label_Vt = Label(self.master, textvariable=self.label_Vt_text,font=self.standard_font, relief=SUNKEN)
        self.label_Vt.grid(row=roww,column=5)
        self.label_Vt.config(bg='white')


        #draw third row
        roww+=1
        self.RR_plus_button = Button(self.master, text=" + ", font=self.huge_font, command= self.RR_plus)
        self.RR_plus_button.grid(row=roww,column=1)
        self.RR_plus_button.config(bg='light blue')

        self.RR_min_button = Button(self.master, text=" - ", font=self.huge_font, command= self.RR_min)
        self.RR_min_button.grid(row=roww,column=2)
        self.RR_min_button.config(bg='light blue')



        self.Vt_plus_button = Button(self.master, text=" + ", font=self.huge_font, command= self.Vt_plus)
        self.Vt_plus_button.grid(row=roww,column=4)
        self.Vt_plus_button.config(bg='light blue')

        self.Vt_min_button = Button(self.master, text=" - ", font=self.huge_font, command= self.Vt_min)
        self.Vt_min_button.grid(row=roww,column=5)
        self.Vt_min_button.config(bg='light blue')


        #draw fourth row (just a spacer)
        roww+=1
        self.label_text = StringVar()
        self.label_text.set("    ")
        self.spacer12 = Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.spacer12.grid(row=roww,column=3)

        #draw fifth row
        roww+=1
        self.vent_start_button = Button(self.master, text="START", font=self.standard_font, command= self.start_vent)
        self.vent_start_button.grid(row=roww,column=1)
        self.vent_start_button.config(bg='light green')


        self.vent_stop_button = Button(self.master, text="STOP", font=self.standard_font, command= self.stop_vent)
        self.vent_stop_button.grid(row=roww,column=5)
        self.vent_stop_button.config(bg='red')



    # RR plus button command
    def RR_plus(self):

        #update vars
        self.RR += 1

        # add gate to prevent crazy high RR!
        if self.RR > 21:
            self.RR = 21

        #redraw label
        self.label_RR_text.set(str(self.RR))

        #write settings/state to file
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'stepper_data.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'alarm_data.json'), 'w') as f:
            json.dump(alarm_dict, f)

    # RR minus button command
    def RR_min(self):

        #update vars
        self.RR -= 1

        # add gate to prevent negative RR!
        if self.RR < 1:
            self.RR = 1

        #redraw label
        self.label_RR_text.set(str(self.RR))

        #write settings/state to file
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'stepper_data.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'alarm_data.json'), 'w') as f:
            json.dump(alarm_dict, f)



    # Vt plus button command
    def Vt_plus(self):

        #update vars
        self.Vt += 50

        # add gate to prevent crazy high RR!
        if self.Vt > 800: #swag system limit which may currently be 700mL?
            self.Vt = 800

        #redraw label
        self.label_Vt_text.set(str(self.Vt))

        #write settings/state to file
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'stepper_data.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'alarm_data.json'), 'w') as f:
            json.dump(alarm_dict, f)

    # Vt minus button command
    def Vt_min(self):

        #update vars
        self.Vt -= 50

        # add gate to prevent negative Vt!
        if self.Vt < 100:
            self.Vt = 100

        #redraw label
        self.label_Vt_text.set(str(self.Vt))

        #write settings/state to file
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'stepper_data.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'alarm_data.json'), 'w') as f:
            json.dump(alarm_dict, f)






    # START VENT!
    def start_vent(self):

        #update vars
        self.RUNNING = True
        self.stepper_mode = 'OPERATE'
        self.alarm_mode = 'OPERATE'

        #redraw label
        self.vent_start_button['state'] = "disable"
        self.vent_stop_button['state'] = "normal"
        self.vent_stop_button.config(bg='red')
        self.vent_start_button.config(bg='gray')

        #write settings/state to file
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'stepper_data.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'alarm_data.json'), 'w') as f:
            json.dump(alarm_dict, f)



    # STOP VENT!
    def stop_vent(self):

        #update vars
        self.RUNNING = False
        self.stepper_mode = 'IDLE'
        self.alarm_mode = 'IDLE'

        #redraw label
        self.vent_stop_button['state'] = "disable"
        self.vent_start_button['state'] = "normal"
        self.vent_start_button.config(bg='light green')
        self.vent_stop_button.config(bg='gray')

        #write settings/state to file
        stepper_dict = {'mode':self.stepper_mode,'resp_rate':self.RR, 'tidal_vol':self.Vt, 'peep':self.PEEP}
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'stepper_data.json'), 'w') as f:
            json.dump(stepper_dict, f)
        with open(os.path.join(self.full_file_dir,'alarm_data.json'), 'w') as f:
            json.dump(alarm_dict, f)



    # alarm mute
    def mute_alarm(self):

        #update vars
        self.MUTE += 1

        #write settings/state to file
        alarm_dict = {'mode':self.alarm_mode, 'mute':self.MUTE}
        with open(os.path.join(self.full_file_dir,'alarm_data.json'), 'w') as f:
            json.dump(alarm_dict, f)


###########################
#MAIN CODE
###########################

#base stuff needed to make it work
root = Tk()

root.geometry("1920x1080")

my_gui = theGUI(root)

root.mainloop()

